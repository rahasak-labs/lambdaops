# Rahasak-Labs Products

1. [Rahasak](https://docs.google.com/document/d/1gpAx3nNRpRLdGbs8WJ_izB-g14y7m2Mq3rUvOVVL9S8/edit) - Highly scalable blockchain storage for big data and IoT

2. [Lekana](https://docs.google.com/document/d/1Z2ube-Ch8pfsE4c3AMl1eTcVf46cluhfy9xeOFucyw8/edit) - Blockchain based document reviewing and approving application

3. [Octopus](https://docs.google.com/document/d/1FEoG_K7E98Dw8cz1bEHcg5-Mdf-axFW0x4Xbjj7g39k/edit) - Blockchain based Secure log storage(audit trail) and analytics platform

4. [Octopus-ML](https://docs.google.com/document/d/14WJQNnKk78vedtoC2xymLkJukx7Wkok4-UNRUeVCUis/edit) - Blockchain based data analytics and machine learning(supports for federated learning) platform

5. [Promize](https://docs.google.com/document/d/1A4_UhNoFYGFhhfdm5QSvKtun2X2gdX4gCgyrXbPkctg/edit) - Peer to peer money transferring application which is an alternative for bank ATMs and Credit/Debit card systems

6. [Siddhi](https://docs.google.com/document/d/1AnoBjyahpCSXmAHOyx6M5D7Vj4hg-D9iaxkWUF1YeW4/edit) - Blockchain based logistic and transport tracking platfrom

7. [Connect](https://docs.google.com/document/d/1s_hc7RtEM58BTixz0c5b0ZoLQTzLvsIlbHdDTpcHtwo/edit) - Blockchain and Self Sovereign Identity(SSI) based digital identity platfrom

8. [Mbank](https://docs.google.com/document/d/1YuriyKq5qQZ4JiNi-GtqKfrIRYEIpuIPcTvgARa4HLU/edit) - Mobile banking application which facilitates doorstep banking concept

9. [Koin](https://docs.google.com/document/d/1_IA6xm2jZwH_qibvFamnwy-qcQp_HB9W9-9huQjupuU/edit) - Blockchain based loyalty point sharing platfrom

10. [Yugala](https://docs.google.com/document/d/1BoiHgHUVdfTo8lyxhrkXzFg-uLOs10A_sfl9KIRVPe4/edit) - Blockchain based encrypted cloud storage

11. [Bloxure](https://docs.google.com/document/d/1F4tcyCR0Xm2W8IavgnXpEhz2hPoyYN1t9KzlsQRgL18/edit) - Blockchain based IoT device monitoring platform

12. Bassa - Scalable blockchian platfrom for Smart Cities

13. Tikiri - Lightweight blockchian platfrom for IoT

14. Casper - Privacy preserving peer to peer transactions system with InterPlanetary File System(IPFS) which is an alternative for blockchain systmes

15. Hora - Blockchain based luxuary watch tracing platfrom
