# Rahasak-Hacks

## Introduction

`gateway` and `validator` are two microservices in the system. `validator` service has a function to validate phone numbers. 
For valid phone no it returns `valid`, for invalid phone no it returns `invalid`. `gateway` service exposes REST API to clients. 
clients can publish phone no's that needs to be validated via gateway REST API as a JSON object. Once validate request received, 
the `gateway` service communicates with validator service to do the phone no validation. `gateway` service sends phone no to 
`validator` service and waiting for the response. Once phone no receives, `validator` service validate it and sends the response back to `gateway` service. When response received from `validator` service to `gateway` service, it returns the response to the client as a JSON object.


![Alt text](rahasak-hacks.png?raw=true "rahasak-hacks architecture")


## Todo

`gateway` and `validator` services need to communicate via `kafka`. `gateway` service sends phone no to `validator` service(via `kafka`).
`validator` service validates phone no and sends the response back to `gateway` service(via kafka). `gateway` service exposes `api/validate` JSON 
REST API to the client. Following are the things to do :)

1. Deploy `kafka` with `Docker` and play around with Kafka
2. Develop gateway and validator services with a language you prefer(`golang/scala/python` would be most welcome)
3. Integrate gateway and validator services with `docker`
4. Deploy dockerized services (`kafka`, `gateway`, `validator`) and test the functionality
5. If you can, try to integrate the services with `kubernetes`
6. Test the REST API functionality with `curl` 
7. Publish the source codes in `gitlab`


## Note

When doing the tasks try to understand what you are doing and what are the behind the scene concepts. Sometimes technologies becoming hypes, they change rapidly. But behind the scene concepts remain unchanged. We believe understanding the behind the 
scene things more important than anything. 

This will be the only technical interview you are having, if you do this we are confident about your skills. Afterwards, you will be invited to have a chat with us. So take your time and do the task. Don't worry if you are not familiar with the technologies/tools we have mentioned. Use google, read about them, do some tutorials and try to complete the tasks.
That's the nature of the industry :)


## Reference

We have written some articles about the above-mentioned topics(`kafka`, `golang`, `scala` integrations). If you want, you are most welcome to refer them. However there are a lot of references available on the internet about these topics,
feel free to google and find things. 

1. [Kafka and Zookeeper with docker](https://medium.com/@itseranga/kafka-and-zookeeper-with-docker-65cff2c2c34f)
2. [Kafka consumer with golang](https://medium.com/@itseranga/kafka-producer-with-golang-fab7348a5f9a)
3. [Kafka producer with golang](https://medium.com/@itseranga/kafka-consumer-with-golang-a93db6131ac2)
4. [Kafka producer with scala and akka](https://medium.com/@itseranga/kafka-producer-with-scala-and-akka-a66d6e132e89)
5. [Kafka consumer with scala and akka streams](https://medium.com/rahasak/kafka-consumer-with-scala-akka-streams-7e3237d6acc)
6. [Kafka/Zookeeper cluster on kubernetes](https://medium.com/rahasak/kafka-zookeeper-cluster-on-kubernetes-43a4aaf27dbb)