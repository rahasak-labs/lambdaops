# Rahasak-Labs

We do research but not satisfy only with publication :). We build products based on our 
researches and take them to the market. Following are the main areas that we are woring on
```
1. Distributed systems
2. Blockchain
3. IoT
4. Big data
5. Machine learning
6. Federated learning
7. Embedded systems
8. Cryptography
```

We love to build most of our stuffs with functional programming. Following are our language
stack
```
1. Scala(akka, akka-streams, cats-effects, doobie, fs2, http4s)
2. Haskell
3. Erlang
4. Golang
```

We use open source tools, do devOps and continuous delivery. Following are some tools that we
are using
```
1. Docker			
2. Kubernetes   
3. Apache Kafka	    		
4. Apache Cassandra    	
5. Etcd				
6. ELK				
7. Puppet	
8. Ansible	
9. Mongodb	
10. GoCd	
11. Lxc		
12. Git		
13. Hbase		
14. Zookeeper		
15. Elassandra	
16. Serverless	
17. Android		
18. IoS			
19. Contik
20. LPWAN/
21. Zigbee
22. BLE	
23. MQTT
24. Micaz
25. Apache spark(spark-ml)
26. Prometheus
27. Grafana
28. Nginx
29. Couchdb
30. Redis
31. IPFS
32. gRPC
```

We following CI/CD approach when building the software products.
```
1. Agile/Scrum
2. TDD
3. Continuous integration/Continuous delivery
```

Finally we love keep everything simple. :)
